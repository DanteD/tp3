/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juegocartas;

/**
 *
 * @author Dante
 */
public class JuegoCartas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        System.out.println("Creando repartidor");
        Repartidor repartidor = new Repartidor("Repartidor");
        
        repartidor.crearMazo();
        
        Thread[] jugadores = new Thread[4];
        
        for (int i = 0; i < 4; i++) {
            System.out.println("Creando jugador" + i);
            Jugador jugador = new Jugador("Jugador " + i, repartidor);
            
            jugador.addObserver(repartidor);
            
            jugadores[i] = new Thread(jugador);
            jugadores[i].start();
            
        }
    }
}
