/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juegocartas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;


/**
 *
 * @author Dante
 */
public class Repartidor implements Observer {
    
    protected String nombre;
    private List<Carta> mazo;
    private List<Jugador> jugadores;
    
    public Repartidor(String nombre) {
        this.nombre = nombre;
        this.mazo = new ArrayList<>();
        this.jugadores = new ArrayList<>();
    }
    
    @Override
    public void update(Observable jugadorObservado, Object object) {
        
        Jugador jugador = (Jugador) jugadorObservado;
        Carta arg = (Carta) object;
        
        if (!this.jugadores.isEmpty()) {
            Jugador ultimoInsertado = this.jugadores.get(this.jugadores.size() - 1);
            
            if (!ultimoInsertado.equals(jugador)) {
                this.jugadores.add(jugador);
            }
        } else {
            this.jugadores.add(jugador);
        }
        
        System.out.println("Jugador " + jugador.getNombre() + " puntos " + jugador.getPuntos());
        
        for(Carta carta : jugador.getMano()){

            if (carta.equals(arg)) {
                System.out.println(carta.getValor() + " de " + carta.getTipo() + " (nueva)");
            } else {
                System.out.println(carta.getValor() + " de " + carta.getTipo());
            }
        }
        
        if(this.mazo.isEmpty()) {
            this.determinadorGanador();
        }
       
    }
    
    public void crearMazo(){
        String palos[] = {"espada","basto","copa","oro"};
        
        for(String palo : palos)
        {
            for(int i = 1;i <= 12; i++)
            {
                this.mazo.add(new Carta(palo,i));
                System.out.println("Se creo el " + i + " de " + palo);   
            }
        }
        
        Collections.shuffle(this.mazo);
        
    }
    
    public synchronized Carta repartirCartas(){
        if(this.mazo.size() > 0){
           
           Carta ultimaCarta = this.mazo.get(this.mazo.size() - 1);
           this.mazo.remove(ultimaCarta);
      
           return ultimaCarta;
        }
        
        return null;
    }
    
    public void determinadorGanador(){
        
        Jugador ganador = null;
        
        
        for(Jugador jugador : this.jugadores){
            
            if(ganador == null){
                
                ganador = jugador;
                
            }
            else if(ganador.getPuntos() < jugador.getPuntos()){
                
                ganador = jugador;
            }
        }
        
        System.out.println("El ganador es el jugador " + ganador.getNombre() + " con " + ganador.getPuntos() + " puntos");
        
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/tp3?user=root&password=&useSSL=false");
            PreparedStatement ps = conn.prepareStatement("INSERT INTO jugadores(nombre, puntos) values (?, ?)");

            ps.setString(1, ganador.getNombre());
            ps.setInt(2,ganador.getPuntos());

            boolean result = ps.execute();
        }catch (Exception e) {
            e.printStackTrace();
        }
    
    }
    
}
