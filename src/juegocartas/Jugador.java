/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juegocartas;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 *
 * @author Dante
 */
public class Jugador extends Observable implements Runnable{
    
    protected String nombre;
    private List<Carta> mano;
    Repartidor repartidor;
    private boolean turno;
    private int puntos;

    public Jugador(String nombre,Repartidor r) {
        this.nombre = nombre;
        this.repartidor = r;
        this.turno = true;
        this.mano = new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Carta> getMano() {
        return mano;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public int getPuntos() {
        return puntos;
    }
     
    @Override
    public void run() {
        
       while(turno == true){
            
            Carta carta = this.repartidor.repartirCartas();
            
            if(carta != null){
                try{
                    this.mano.add(carta);
                    this.puntos = carta.getValor() + this.puntos;
                    this.setChanged();
                    this.notifyObservers(carta);
                    
                    Thread.sleep(2000);
                }
                catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error agregando carta");
                }

            }
            else {
                this.turno = false;
            }
        }        
    }
    
    
}
